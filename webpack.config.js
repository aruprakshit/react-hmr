module.exports = {
  entry: "./index.js",
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel",
      include: __dirname,
      query: {
        presets: [ 'es2015', 'react', 'react-hmre' ]
      }
    }]
  },
  output: {
    path: __dirname + "/dist",
    filename: "bundle.js",
    publicPath: "/static/"
  }
}
